package com.yorbit.sbp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yorbit.sbp.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Optional<Product> findByProductId(Long id);
	
}
