package com.yorbit.sbp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yorbit.sbp.model.Cart;

public interface CartRepository extends JpaRepository<Cart, Long> {

	Cart findByUserIdAndStatus(Long userId,String status);

	List<Cart> findByUserId(Long userId);

}

