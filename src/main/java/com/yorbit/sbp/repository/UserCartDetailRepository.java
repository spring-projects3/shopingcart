package com.yorbit.sbp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.yorbit.sbp.model.Cart;
import com.yorbit.sbp.model.UserCartDetail;

@Repository
public interface UserCartDetailRepository extends JpaRepository<UserCartDetail, Long> {
	
	UserCartDetail findByCartIdAndProductId(Long cartId, Long productId);
	
	// List<UserCartDetail> findByCartId(Long cartId);
	
	// List<UserCartDetail> findByCart(Cart cart);
	
	// List<UserCartDetail> findByCartAndProductId(Cart cart, Long productId);
	// UserCartDetail findByCartAndProductId(Cart cart, Long productId);
	// @Transactional
	// void deleteByCartAndProductId(Cart cart, Long productId);
	
	@Modifying
	@Query("delete from UserCartDetail u where u.cartId=?1 and u.productId=?2")
	void deleteByCartIdAndProductId(Long cartId, Long productId);
	
}
