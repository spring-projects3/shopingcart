package com.yorbit.sbp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yorbit.sbp.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	
}
