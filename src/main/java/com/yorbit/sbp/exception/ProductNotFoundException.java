package com.yorbit.sbp.exception;

public class ProductNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public ProductNotFoundException() {
		super();
	}
	
	public ProductNotFoundException(String message) {
		this.message = message;
	}
}
