package com.yorbit.sbp.exception;

import javax.validation.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CartException {
	
	@ExceptionHandler(value = ProductNotFoundException.class)
	public ResponseEntity<Object> exception(ProductNotFoundException pnf) {
		return new ResponseEntity<>(pnf.message, HttpStatus.NOT_FOUND);
		
	}
	
	@ExceptionHandler(value = ValidationException.class)
	public ResponseEntity<Object> validationError(ValidationException ve) {
		return new ResponseEntity<>("Request Validation Exception.", HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value=CartError.class)
	public ResponseEntity<Object> cartError(CartError ce){
		return new ResponseEntity<>(ce.message,HttpStatus.NOT_FOUND);
	}
}
