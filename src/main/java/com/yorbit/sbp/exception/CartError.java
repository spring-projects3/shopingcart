package com.yorbit.sbp.exception;

public class CartError extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String message;
	
	public CartError() {
		super();
	}
	
	public CartError(String message) {
		this.message = message;
	}
}
