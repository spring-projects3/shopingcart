package com.yorbit.sbp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		// TODO Auto-generated method stub
		return builder.sources(Application.class).initializers();
	}
	
	public static void main(String[] args) {		
		final SpringApplication springApplication = new SpringApplication(Application.class);
//		springApplication.addInitializers();
		springApplication.run(args);		
//		SpringApplication.run(Application.class, args);
	}
	
}
