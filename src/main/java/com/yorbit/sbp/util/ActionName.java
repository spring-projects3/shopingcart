package com.yorbit.sbp.util;

import java.util.Arrays;

public enum ActionName {
	ADD, 
	REMOVE, 
	DELETE;
	
	public static boolean isValidAction(String action) {
		return Arrays.asList(ActionName.values()).contains(ActionName.valueOf(action));
		
	}
}
