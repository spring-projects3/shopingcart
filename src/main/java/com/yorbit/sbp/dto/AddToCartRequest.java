package com.yorbit.sbp.dto;

import java.io.Serializable;



public class AddToCartRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long userId;
//	@NotNull(message="missed cartId")
	private Long cartId;
	private Long productId;
	private Integer quantity;
	private Double productPrice;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getCartId() {
		return cartId;
	}
	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
	
	
	
/*	@Override
	public String toString() {
		return "AddToCart [userId=" + userId + ", productId=" + productId + ", quantity=" + quantity + ", productPrice=" + productPrice
				+ "]";
	}*/
	
}
