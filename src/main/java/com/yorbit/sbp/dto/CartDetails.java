package com.yorbit.sbp.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Digits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yorbit.sbp.model.Product;


public class CartDetails {
	

	private Long UserId;
	@JsonProperty("total_price")
	@Digits(integer = 36, fraction = 2, message = "totalPrice size should not exceed 36 characters")
	private BigDecimal totalPrice;
	private List<Product> products = new ArrayList<>();
	
	public Long getUserId() {
		return UserId;
	}
	public void setUserId(Long userId) {
		UserId = userId;
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
