package com.yorbit.sbp.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.yorbit.sbp.model.Cart;
import com.yorbit.sbp.model.User;



@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CartDetailsResponse {
	private User user;
	private Cart cart;
	private List<CartDetails> cartDetails;
	private String message;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	public List<CartDetails> getCartDetails() {
		return cartDetails;
	}
	public void setCartDetails(List<CartDetails> cartDetails) {
		this.cartDetails = cartDetails;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
