package com.yorbit.sbp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="book")
public class Book {
	@Id
	@Column(name = "bookid", unique = true, nullable = false, precision = 20)
	@GeneratedValue(generator = "BOOKID_GENERATOR")
	@GenericGenerator(name="BOOKID_GENERATOR", strategy = "native")
	private Long bookId;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productid", nullable = false)
	private Product product;
	
	@Column(name="genres")
	private String genres;
	
	@Column(name="authour")
	private String authour;
	
	@Column(name="publications")
	private String publications;

	public Long getBookId() {
		return bookId;
	}
	
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public Product getProduct() {
		return product;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}

	public String getGenres() {
		return genres;
	}

	public void setGenres(String genres) {
		this.genres = genres;
	}
	
	public String getAuthour() {
		return authour;
	}
	
	public void setAuthour(String authour) {
		this.authour = authour;
	}

	public String getPublications() {
		return publications;
	}	
	
	public void setPublications(String publications) {
		this.publications = publications;
	}
}
