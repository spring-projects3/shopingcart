package com.yorbit.sbp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;




@Entity
@Table(name="apparel")
public class Apparal {
	
	@Id
	@Column(name="apparelid",unique=true,nullable=false ,precision=20)
	@GeneratedValue(generator="APPARELID_GENERATOR")
	@GenericGenerator(name="APPARELID_GENERATOR",strategy="native")
	private Long apparelId;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productid", nullable = false)
	private Product product;
	
	@Column(name="type")
	private String type; 
	
	@Column(name="brand")
	private String brand;
	
	@Column(name="design")
	private String design;
	
	public Long getApparelId() {
		return apparelId;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public String getType() {
		return type;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public String getDesign() {
		return design;
	}

	public void setApparelId(Long apparelId) {
		this.apparelId = apparelId;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setDesign(String design) {
		this.design = design;
	}
	
	
	
}
