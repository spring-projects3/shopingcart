package com.yorbit.sbp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="usercartdetail")

public class UserCartDetail {

	
	@Id
	@Column(name="cartdetailid",unique=true,nullable=false,precision=20)
	@GeneratedValue(generator="CARTDETAILID_GENERATOR")
	@GenericGenerator(name="CARTDETAILID_GENERATOR",strategy="native")
	private Long cartDetailId;
	
	@JsonIgnore
//	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cartid", nullable = false)
	@Column(name="cartid")
	private Long cartId; 
	
/*	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cartid", nullable = false)
//	@JsonBackReference
	private Cart cart;*/
	
	@Column(name="productid")
	private Long productId;
	@Column(name="price")
	private double price;
	@Column(name="quantity")
	private int quantity;
	@Column(name="discountamount")
	private double discountAmount;
	@Column(name="totalprice")
	private double totalPrice;
	public Long getCartDetailId() {
		return cartDetailId;
	}
	public void setCartDetailId(Long cartDetailId) {
		this.cartDetailId = cartDetailId;
	}
	public Long getCartId() {
		return cartId;
	}
	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
}
