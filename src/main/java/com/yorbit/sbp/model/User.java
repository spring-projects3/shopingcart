package com.yorbit.sbp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user")

public class User {
	@Id
	@Column(name = "userid", unique = true, nullable = false, precision = 20)
	@GeneratedValue(generator = "USERID_GENERATOR")
	@GenericGenerator(name = "USERID_GENERATOR", strategy = "native")
	private Long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private Long phone;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String country;
	private String zipcod;
	
	/*public Long getUserId() {
		return userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public Long getPhone() {
		return phone;
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}
	
	public String getAddressLine2() {
		return addressLine2;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getZipcod() {
		return zipcod;
	}*/
}
