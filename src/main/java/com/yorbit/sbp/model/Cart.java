package com.yorbit.sbp.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cart")
public class Cart {
	
	@Id
	@Column(name = "cartid", unique = true, nullable = false, precision = 20)
	@GeneratedValue(generator = "CARTID_GENERATOR")
	@GenericGenerator(name = "CARTID_GENERATOR", strategy = "native")
	private Long cartId;
	
/*	@Column(name="productid")
	private Long productId;*/
	
	@Column(name="userid")
	private Long userId;
	
	
	@Column(name="status")
	private String status;
	
	@OneToMany(fetch = FetchType.EAGER, targetEntity = UserCartDetail.class, mappedBy = "cartId", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<UserCartDetail> userCartDetails = new HashSet<>();

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<UserCartDetail> getUserCartDetails() {
		return userCartDetails;
	}

	public void setUserCartDetails(Set<UserCartDetail> userCartDetails) {
		this.userCartDetails = userCartDetails;
	}
	
/*	@OneToOne(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
	private Set<User> user = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<Product> product = new HashSet<>(0);*/
	
	
}
