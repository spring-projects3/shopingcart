package com.yorbit.sbp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


//@Getter
@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6755616970766594966L;
	
	@Id
	@Column(name = "productid", unique = true, nullable = false, precision = 20)
	@GeneratedValue(generator = "producId_GENERATOR")
	@GenericGenerator(name = "producId_GENERATOR", strategy = "native")
	private Long productId;
	
	@Column(name = "productname")
	private String productName;
	/*
	 * @Column(name = "lastname") private String lastname;
	 * 
	 * @Column(name = "email") private String email;
	 * 
	 * @Column(name = "age") private Integer age;
	 * 
	 */
	
	@Column(name = "price")
	private Integer price;
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Book book;
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Apparal apparal;
	
	// @OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	// private Set<Apparal> apparal = new HashSet<>(0);
	
	public Long getProducId() {
		return productId;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public Integer getPrice() {
		return price;
	}
	
	public void setBook(Book book) {
		this.book = book;
	}
	
	public Book getBook() {
		return book;
	}
	
	public void setApparal(Apparal apparal) {
		this.apparal = apparal;
	}
	
	public Apparal getApparal() {
		return apparal;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
}
