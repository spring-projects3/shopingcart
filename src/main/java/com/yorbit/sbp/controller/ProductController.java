package com.yorbit.sbp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yorbit.sbp.model.Product;
import com.yorbit.sbp.service.ProductService;

@RestController
@RequestMapping("/")
public class ProductController {
	
	@Autowired(required = true)
	private transient ProductService productService;
	

	public ProductController(ProductService productService) {
		
	}
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("/product")
	public List<Product> getProducts(){
		return productService.getProducts();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/product/{id}")
	public Product getProducts(@PathVariable Long id){
		return productService.getProducts(id);
	}
	
	/**
	 * 
	 * @param product
	 * @return
	 */
	@PostMapping("/product")	
	public Product addProduct(Product product){
		return productService.addProducts(product);
	}
	
	
}
