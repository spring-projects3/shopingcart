package com.yorbit.sbp.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yorbit.sbp.dto.AddToCartRequest;
import com.yorbit.sbp.dto.CartDetailsResponse;
import com.yorbit.sbp.dto.UpdateCartRequest;
import com.yorbit.sbp.model.Cart;
import com.yorbit.sbp.service.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {
	
	Logger logger =  LoggerFactory.getLogger(CartController.class);
	
	
	@Autowired(required = true)
	private transient CartService cartService;
	
	/**
	 * Get the cart details by user id
	 * @param userId
	 * @return
	 */
	@GetMapping("/user/{userId}")
	public CartDetailsResponse getCart(@PathVariable Long userId){		
		return cartService.getCartByUser(userId);	
		
	}
	
	/**
	 * add product details to user cart
	 * @param addToCart
	 * @return
	 */
	@PostMapping
	public CartDetailsResponse addProductToCart(@Valid @RequestBody AddToCartRequest addToCart){
		logger.info(addToCart.getProductId()+"-----"+addToCart.getUserId());
		return cartService.addProductToCart(addToCart);
	}
	
	/**
	 * update the user cart product details
	 * @param updateCart
	 * @return
	 */
	@PutMapping
	public CartDetailsResponse updateCart(@Valid @RequestBody UpdateCartRequest updateCart){
		logger.info("Action name : {} , productId : {} , userId : {} ", updateCart.getActionName(),updateCart.getProductId(),updateCart.getUserId());
		return cartService.updateCart(updateCart);
	}
	
	/**
	 * delete cart by cart id
	 * @param cartId
	 * @return
	 */
	@DeleteMapping("/{cartId}")
	public boolean deleteCart(@PathVariable Long cartId){
		logger.info("card id : {}",cartId);
		return cartService.deleteCart(cartId);
		
	}
	/**
	 * Delete product details from user cart using user id product id and quantity
	 * @param userId
	 * @param id
	 * @param quantity
	 * @return
	 */
	@DeleteMapping("/delete/user/{userId}/productid/{id}/quantity/{quantity}")
	public Cart deleteProductFromCart(@PathVariable Long userId,@PathVariable Long id,@PathVariable Long quantity){
		return cartService.deleteProductFromCart(userId,id,quantity);
	}
	

	
	
}
