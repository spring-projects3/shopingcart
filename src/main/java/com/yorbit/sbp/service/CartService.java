package com.yorbit.sbp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yorbit.sbp.dto.AddToCartRequest;
import com.yorbit.sbp.dto.CartDetails;
import com.yorbit.sbp.dto.CartDetailsResponse;
import com.yorbit.sbp.dto.UpdateCartRequest;
import com.yorbit.sbp.exception.CartError;
import com.yorbit.sbp.model.Cart;
import com.yorbit.sbp.model.UserCartDetail;
import com.yorbit.sbp.repository.CartRepository;
import com.yorbit.sbp.repository.UserCartDetailRepository;
import com.yorbit.sbp.util.ActionName;

@Service
public class CartService {
	
	Logger logger = LoggerFactory.getLogger(CartService.class);
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private UserCartDetailRepository userCartDetailRepository;
	
	/**
	 * 
	 * @param addToCart
	 * @return
	 */
	public CartDetailsResponse addProductToCart(AddToCartRequest addToCart) {
		
		Cart cart = cartRepository.findByUserIdAndStatus(addToCart.getUserId(), "A");
		if (!Optional.ofNullable(cart).isPresent()) {
			cart = new Cart();
			cart.setStatus("A");
			cart.setUserId(addToCart.getUserId());
			Cart cartObj = cartRepository.saveAndFlush(cart);
			
			UserCartDetail userCartDetail = new UserCartDetail();
			userCartDetail.setCartId(cartObj.getCartId());
			userCartDetail.setProductId(addToCart.getProductId());
			userCartDetail.setPrice(addToCart.getProductPrice());
			userCartDetail.setTotalPrice(addToCart.getProductPrice() * addToCart.getQuantity());
			userCartDetail = userCartDetailRepository.saveAndFlush(userCartDetail);
			logger.info("Carddetail id : " + userCartDetail.getCartDetailId());
			
		} else {
			addProductToExistingCart(addToCart, cart);
			
		}
		cartRepository.flush();
		return getCartByUser(addToCart.getUserId());	
		
	}

	private void addProductToExistingCart(AddToCartRequest addToCart, Cart cart) {
		UserCartDetail userCartDetail = userCartDetailRepository.findByCartIdAndProductId(cart.getCartId(), addToCart.getProductId());
		Cart cart1 = cart;			
		if (Optional.ofNullable(userCartDetail).isPresent() && userCartDetail.getProductId() == addToCart.getProductId()) {
			logger.info(" Add exiting product and update count + price");
			userCartDetail.setQuantity(userCartDetail.getQuantity() + addToCart.getQuantity());
			userCartDetail.setTotalPrice(userCartDetail.getTotalPrice() + (addToCart.getProductPrice() * addToCart.getQuantity()));
			userCartDetailRepository.saveAndFlush(userCartDetail);
		} else {
			logger.info("Add new product and update price");
			userCartDetail = new UserCartDetail();
			userCartDetail.setCartId(cart1.getCartId());
			userCartDetail.setProductId(addToCart.getProductId());
			userCartDetail.setPrice(addToCart.getProductPrice());
			userCartDetail.setQuantity(addToCart.getQuantity());
			userCartDetail.setTotalPrice(addToCart.getProductPrice() * addToCart.getQuantity());
			cart.getUserCartDetails().add(userCartDetail);
			cartRepository.saveAndFlush(cart);
		}
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public CartDetailsResponse getCartByUser(Long userId) {
		
		CartDetailsResponse cdResponse = new CartDetailsResponse();		
		Cart cart = cartRepository.findByUserIdAndStatus(userId,"A");
		if(!Optional.ofNullable(cart).isPresent()){
			 cdResponse.setMessage("User has no cart!!");
			 return cdResponse;
		}
		cdResponse.setCart(cart);	
//		List<CartDetails> detailList = new ArrayList<CartDetails>();		
		Optional<Set<UserCartDetail>>  ss = Optional.ofNullable(cart.getUserCartDetails());		
		if(ss.isPresent()){
		ss.get().stream().forEach(ww->{
			logger.info("card detail id : "+ww.getCartDetailId());
		});
		}		
		return cdResponse;		
	}

	/**
	 * 
	 * @param userId
	 * @param productId
	 * @param quantity
	 * @return
	 */
	@Transactional
	public Cart deleteProductFromCart(Long userId, Long productId, Long quantity) {
		logger.info(userId + "---" + productId);
		Cart cart = cartRepository.findByUserIdAndStatus(userId, "A");
		Optional.ofNullable(cart).orElseThrow(() -> new CartError("Cart does not exists"));		
		UserCartDetail userCartDetail = userCartDetailRepository.findByCartIdAndProductId(cart.getCartId(), productId);
		Optional.ofNullable(userCartDetail).orElseThrow(() -> new CartError("User Cart Detail does not exist"));
		Long prodQuantity = (userCartDetail.getQuantity() - quantity);
		
		if (prodQuantity.intValue() > 0) {
			userCartDetail.setQuantity(prodQuantity.intValue());
			userCartDetailRepository.saveAndFlush(userCartDetail);
		} else {
			userCartDetailRepository.delete(userCartDetail);
			userCartDetailRepository.deleteByCartIdAndProductId(userCartDetail.getCartId(),productId);
			entityManager.flush();
			entityManager.clear();			
		}		
		cart = cartRepository.findByUserIdAndStatus(userId, "A");
		return cart;
	}

	/**
	 * 
	 * @param updateCart
	 * @return
	 */
	public CartDetailsResponse updateCart(@Valid UpdateCartRequest updateCart) {
		if(!ActionName.isValidAction(updateCart.getActionName())){
			throw new CartError("Not a valid action type(ADD/REMOVE/DELETE)");
		}
		if (updateCart.getActionName().equalsIgnoreCase(ActionName.ADD.name())) {
			AddToCartRequest addToCart = new AddToCartRequest();			
			BeanUtils.copyProperties(updateCart, addToCart);			
			return addProductToCart(addToCart);
		}else if (updateCart.getActionName().equalsIgnoreCase(ActionName.REMOVE.name())){
			
			deleteProductFromCart(updateCart.getUserId(),updateCart.getProductId(),Long.valueOf(updateCart.getQuantity()));
			return getCartByUser(updateCart.getUserId());
		}else{
			deleteProductFromCart(updateCart.getUserId(),updateCart.getProductId(),Long.valueOf(updateCart.getQuantity()));
			return getCartByUser(updateCart.getUserId());
		}
				
		
	}

	/**
	 * 
	 * @param cartId
	 * @return
	 */
	@Transactional
	public Boolean deleteCart(Long cartId) {		
		cartRepository.delete(cartId);
		return true;
	}
	
}
