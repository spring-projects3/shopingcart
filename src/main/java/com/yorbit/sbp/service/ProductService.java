package com.yorbit.sbp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.yorbit.sbp.exception.ProductNotFoundException;
import com.yorbit.sbp.model.Product;
import com.yorbit.sbp.repository.ProductRepository;

@Component
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	public List<Product> getProducts() {
		List<Product> products = productRepository.findAll();
		return products;
	}
	
	public Product addProducts(Product product) {
		validate(product);
		return productRepository.saveAndFlush(product);
	}
	
	private void validate(Product product) {
		Optional.ofNullable(product.getProductName()).orElseThrow(() -> new ProductNotFoundException("Product Name is null"));
	}
	
	public Product getProducts(Long id) {
		Optional<Product> product = productRepository.findByProductId(id);
		product.orElseThrow(() -> new ProductNotFoundException("Product not found"));
		return product.get();
	}
	
}
