package com.mindtree.sgb.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yorbit.sbp.Application;
import com.yorbit.sbp.model.Apparal;
import com.yorbit.sbp.model.Book;
import com.yorbit.sbp.model.Product;
import com.yorbit.sbp.repository.ProductRepository;
import com.yorbit.sbp.service.ProductService;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ProductServiceTest {
	
	@MockBean
	ProductRepository productRepository;
	
	@Autowired
	ProductService productServie;
	
	@Test
	public void TestGetProducts(){
	
		List<Product> products = new ArrayList<Product>();
		createProductObj(products);
		Mockito.when(productRepository.findAll()).thenReturn(products);
		Assert.assertEquals(1, productServie.getProducts().size());
		
	}
	
	@Test
	public void TestGetProductById(){
		Optional<Product> product =  Optional.of(createProduct());		
		Mockito.when(productRepository.findByProductId(Mockito.any(Long.class))).thenReturn(product);
		Assert.assertNotNull(productServie.getProducts(1L));
	}
	
	@Test
	public void TestAddProducts(){
		Product product = createProduct();
		Mockito.when(productRepository.saveAndFlush(Mockito.any(Product.class))).thenReturn(product);
		Assert.assertNotNull(productServie.addProducts(product));
	}
	

	private void createProductObj(List<Product> products) {
		Product product = createProduct();
		products.add(product);
	}

	private Product createProduct() {
		Product product = new Product();
		Book book = new Book();
		book.setAuthour("sai");
		book.setBookId(1L);
		book.setGenres("fiction");
		book.setPublications("sai publications");		
		product.setBook(book);
		
		Apparal apparal = new Apparal();
		apparal.setApparelId(1l);
		apparal.setBrand("ADDIDAS");
		apparal.setDesign("international");
		apparal.setType("casual");
		
		product.setProductId(1L);
		product.setProductName("HarryPorter");
		product.setPrice(10);
		product.setApparal(apparal);
		return product;
	}
	
	
	
}
