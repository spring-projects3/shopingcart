package com.mindtree.sgb.service;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.yorbit.sbp.Application;
import com.yorbit.sbp.dto.AddToCartRequest;
import com.yorbit.sbp.dto.CartDetailsResponse;
import com.yorbit.sbp.dto.UpdateCartRequest;
import com.yorbit.sbp.model.Cart;
import com.yorbit.sbp.model.UserCartDetail;
import com.yorbit.sbp.repository.CartRepository;
import com.yorbit.sbp.repository.UserCartDetailRepository;
import com.yorbit.sbp.service.CartService;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class CartServiceTest {
	
	@Autowired
	CartService cartService;
	
	@MockBean
	private CartRepository cartRepository;
	
	@MockBean
	private UserCartDetailRepository userCartDetailRepository;
	
	@Mock
	CartDetailsResponse cdResponse;
	
	@Mock
	List<Cart> list = new LinkedList<>();
	@Autowired
    ApplicationContext context;
	
	
	@Test
	public void TestGetCartByUser() {
		Cart cart = getCartMockObject();
		Mockito.when(cartRepository.findByUserIdAndStatus(1L, "A")).thenReturn(cart);
		CartDetailsResponse cdRes =  cartService.getCartByUser(1L);
		Assert.notNull(cdRes);//cartService.getCartByUser(1L));
	}


	@Test
	public void TestAddProductToCart(){
		AddToCartRequest  cartReq= createCartRequest();		
		Cart cart = getCartMockObject();
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(null);
		Mockito.when(cartRepository.saveAndFlush(Mockito.any(Cart.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.saveAndFlush(Mockito.any(UserCartDetail.class))).thenReturn(createUserCartDetails());
		Assert.notNull(cartService.addProductToCart(cartReq));
	}
	
	
	@Test
	public void TestAddProductToExistingCart(){
		AddToCartRequest  cartReq= createCartRequest();		
		Cart cart = getCartMockObject();
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(cartRepository.saveAndFlush(Mockito.any(Cart.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class),Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Assert.notNull(cartService.addProductToCart(cartReq));
	}

	
	@Test
	public void TestDeleteProductFromCart(){
		Cart cart = getCartMockObject();
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class), Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class),Mockito.any(String.class))).thenReturn(cart);
		Assert.notNull(cartService.deleteProductFromCart(1L, 1L, 1L));
		
	}
	
	
	@Test
	public void TestDeleteProductFromCart1(){
		Cart cart = getCartMockObject();		
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class), Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class),Mockito.any(String.class))).thenReturn(cart);
		Assert.notNull(cartService.deleteProductFromCart(1L, 1L, 0L));
		
	}
	
	@Test
	public void TestupdateCartForAdd(){
		Cart cart = getCartMockObject();
	
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(cartRepository.saveAndFlush(Mockito.any(Cart.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class),Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Assert.notNull(cartService.updateCart(createUpdateCartRequest()));
		
	}
	@Transactional
	@Test
	public void TestupdateCartForRemove(){
		Cart cart = getCartMockObject();
		UpdateCartRequest updateReq = createUpdateCartRequest();
		updateReq.setActionName("REMOVE");
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class), Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class),Mockito.any(String.class))).thenReturn(cart);
		Assert.notNull(cartService.updateCart(updateReq));
		
	}
	
	@Transactional
	@Test
	public void TestupdateCartForDelete(){
		Cart cart = getCartMockObject();
		UpdateCartRequest updateReq = createUpdateCartRequest();
		updateReq.setActionName("DELETE");
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class), Mockito.any(String.class))).thenReturn(cart);
		Mockito.when(userCartDetailRepository.findByCartIdAndProductId(Mockito.any(Long.class), Mockito.any(Long.class))).thenReturn(createUserCartDetails());
		Mockito.when(cartRepository.findByUserIdAndStatus(Mockito.any(Long.class),Mockito.any(String.class))).thenReturn(cart);
		Assert.notNull(cartService.updateCart(updateReq));
		
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void TestupdateCart(){
		Cart cart = getCartMockObject();
		UpdateCartRequest updateReq = createUpdateCartRequest();
		updateReq.setActionName("INSERT");
		Assert.notNull(cartService.updateCart(updateReq));
		
	}
	
	
	private UpdateCartRequest createUpdateCartRequest() {
		
		UpdateCartRequest updateCartRequest = new UpdateCartRequest();
		updateCartRequest.setActionName("ADD");
		updateCartRequest.setCartId(1L);
		updateCartRequest.setProductId(1L);
		updateCartRequest.setProductPrice(10.00);
		updateCartRequest.setQuantity(1);
		updateCartRequest.setUserId(1L);
		return updateCartRequest;
		
	}


	private Cart getCartMockObject() {
//		List<Cart> list1 = new LinkedList<>();
		Cart cart = new Cart();
		cart.setCartId(1L);
		cart.setStatus("Active");
		cart.setUserId(123456l);
		cart.setUserCartDetails(createCartDetails());
//		list1.add(cart);
		return cart;
	}
	
	
	private Set<UserCartDetail> createCartDetails() {
		Set<UserCartDetail> userCartDetails = new HashSet<>();
		UserCartDetail userCartDetail = createUserCartDetails();
		userCartDetails.add(userCartDetail);
		return userCartDetails;
	}


	private UserCartDetail createUserCartDetails() {
		UserCartDetail userCartDetail = new UserCartDetail();	
		userCartDetail.setCartDetailId(1L);
		userCartDetail.setCartId(1L);		
//		userCartDetail.setCart(cartObj);
		userCartDetail.setProductId(1L);
		userCartDetail.setPrice(10.00);
		userCartDetail.setTotalPrice(10.00);
		return userCartDetail;
	}


	private AddToCartRequest createCartRequest() {
		AddToCartRequest  cartReq = new AddToCartRequest();
		cartReq.setCartId(1L);
		cartReq.setUserId(123456L);
		cartReq.setProductId(3L);
		cartReq.setProductPrice(10.00);
		cartReq.setQuantity(1);
		return cartReq;
	}

	
	
}
