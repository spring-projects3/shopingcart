package com.mindtree.sgb.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.yorbit.sbp.Application;
import com.yorbit.sbp.controller.CartController;
import com.yorbit.sbp.dto.AddToCartRequest;
import com.yorbit.sbp.dto.CartDetailsResponse;
import com.yorbit.sbp.dto.UpdateCartRequest;
import com.yorbit.sbp.model.Cart;

@SuppressWarnings("deprecation")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class CartControllerTest {
	
	@Autowired
	CartController cartController;
	
	@Test
	public void TestGetCart() {
		CartDetailsResponse cartDetailsResponse = cartController.getCart(1L);
		Assert.notNull(cartDetailsResponse);
	}
	
	@Test
	public void TestAddProductToCart() {
		
		AddToCartRequest cartReq = new AddToCartRequest();
		cartReq.setProductId(3L);
		cartReq.setCartId(1L);
		cartReq.setQuantity(1);
		cartReq.setProductPrice(20.00);
		
		CartDetailsResponse cartDetailsResponse = cartController.addProductToCart(cartReq);
		Assert.notNull(cartDetailsResponse);
	}
	
	@Test
	public void TestUpdateCart() {
		
		AddToCartRequest cartReq = new AddToCartRequest();
		cartReq.setProductId(3L);
		cartReq.setCartId(1L);
		cartReq.setQuantity(1);
		cartReq.setProductPrice(20.00);
		
		CartDetailsResponse cartDetailsResponse = cartController.updateCart(createUpdateCartRequest());
		Assert.notNull(cartDetailsResponse);
	}
	
	@Test
	public void TestDeleteCart() {
		Assert.isTrue(cartController.deleteCart(5L));
	}
	
	@Test
	public void TestDeleteProductFromCart() {
		 Cart cart  = cartController.deleteProductFromCart(1L, 3L, 1L);
		 Assert.notNull(cart);
	}
	
	
	private UpdateCartRequest createUpdateCartRequest() {
		
		UpdateCartRequest updateCartRequest = new UpdateCartRequest();
		updateCartRequest.setActionName("ADD");
		updateCartRequest.setCartId(3L);
		updateCartRequest.setProductId(3L);
		updateCartRequest.setProductPrice(20.00);
		updateCartRequest.setQuantity(1);
		updateCartRequest.setUserId(1L);
		return updateCartRequest;
		
	}
	
}
